import { MyMap } from "./MyMap";

jest.mock('mapbox-gl/dist/mapbox-gl', () => ({
  Map: jest.fn(() => ({
    on: (e: string, cb1: any, cb2?: any) => {
      if (e === 'load') {
        cb1();
      }

      if (e === 'click' && cb2) {
        cb2({points: {lat: 1, lng: 2}});
      }
    },

    queryRenderedFeatures: () => [
      {
        id: 1234,
        properties: {
          class: "park_like",
          filterrank: 2,
          maki: "park",
          name: "Dumbarton House",
          type: "historic house"
        },
        geometry: {
          coordinates: []
        }
      },
    ],
  })),
}));

describe('MapboxGl', () => {
  it('should register event listeners', () => {
    const container = document.createElement("div");
    const addPOIMock = jest.fn();
    new MyMap(container, {addPOI: addPOIMock});
    expect(addPOIMock).toHaveBeenCalledWith({id: 1234, name: 'Dumbarton House', type: 'historic house', coordinates: []});
  });
});