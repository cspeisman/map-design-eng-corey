import * as React from 'react';
import { useState } from 'react';
import { MyMap } from "../map/MyMap";
import './App.css';
import { MapInterface } from "../map/types";
import { FavoritesList } from "./Favorites/FavoritesList";
import { Favorite } from "./Favorites/types";
import { MapView } from "./MapView";

export function App() {
  const [map, setMap] = useState<MapInterface | null>(null);
  const [favorites, setFavorites] = useState<Favorite[]>([]);

  const addPOIToFavorites = (poi: Favorite) => setFavorites((prev) => [...prev, poi]);

  const deleteFav = (id: number) => setFavorites((prev) => prev.filter(f => f.id !== id));

  const createMap = (container: HTMLElement) => setMap(new MyMap(container, {addPOI: addPOIToFavorites}));

  return (
    <div className="App">
      {map && <FavoritesList  favorites={favorites} map={map} deleteFav={deleteFav}/>}
      <MapView createMap={createMap} />
    </div>
  );
}
