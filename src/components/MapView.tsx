import * as React from 'react';

interface Props {
  createMap: (container: HTMLElement) => void;
}

export class MapView extends React.Component<Props> {
  private mapContainer = React.createRef<HTMLDivElement>();

  componentDidMount = () => {
    const containerEl = this.mapContainer;
    if (containerEl && containerEl.current) {
      this.props.createMap(containerEl.current)
    }
  };

  render() {
    return <div ref={this.mapContainer} className="map-container"/>;
  }
}

