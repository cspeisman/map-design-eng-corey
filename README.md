# Map Design Engineering Challenge
## Assignment
We'd like to build a point-of-interest (POI) favoriting system. We want users to be able to click on a POI to add it to a list of favorite places, and to display that list with a little information about what that place is, like a name and type. The user should be able to move the map from favorite to favorite by clicking items in the list. The user should also be able to unfavorite an item by clicking a button in the favorites list. 

We'd like you to build the following features: 
* Add a POI to a list of favorites by clicking on the map
* Move the map from favorite to favorite by clicking through the list
* Unfavorite POIs

We'd also like a small write-up that explains any decisions and tradeoffs you made. If you included any dependencies, spend some time talking about why you chose them, and crucially, what the downsides are of including them.

### What we're looking for
* Your solutions should be extensible and re-usable
* Your code should be well tested
* Your code should be tidy and adhere to conventions
* While a perfect design is not necessary, your UI should be usable
* Your write-up should be thoughtful and coherent

## Development
This project was bootstrapped with Create React App.

Run `npm install` or `yarn install` to install dependencies.

Run `npm start` to start the app in development mode.

Run `npm test` to run tests.